package Config;
/*
 * @author Felype 10/09/2020
 */
public class Settings {
	
	/*
	 * Info's
	 */
	public static final String NAME = "Concessionaria do Jão";
	public static final String[] CONTATO = 
		{ "WhatsApp: +55 85 93432-2444"
		 ,"Email: concedojao@jao.com"
		 ,"Site: concedojao.com\n" };
	public static final String[] PENSAMENTOS =
		{ "Que loja linda da peste...."
		 ,"Deus me livre, mas quem me dera..."
		 ,"Aquele ali era o que eu queria...."
		 ,"Preciso arranjar um trampo melhor."
		 ,"Nsssss que carrão!!!" };
	
	/*
	 * Carro
	 */
	
	public static final String[] MARCAS = { "BMW -> R$40.000,00", "FERRARI -> R$90.000,00", "FIAT -> R$15.000,00", "FORD -> R$25.000,00\n" };
	public static final String[] COR = { "BRANCO -> R$520,00", "PRETO R$520,00", "VERMELHO R$620,00", "AZUL R$620,00\n" };
	public static final String[] MODELOS_BMW = { "X5 -> R$462.000,00", "Z4 -> R$309.950,00", "I3 -> R$279.563,00", "X4 -> R$419.020,00\n" };
	public static final String[] MODELOS_FERRARI = { "488 -> R$2.352.000,00", "Portofino -> R$924.430,00", "F8 -> R$976.860,00", "GTC4 -> R$3.252.000,00 \n" };
	public static final String[] MODELOS_FIAT = { "Dobló -> R$67.990,00", "Argo -> R$39.990,00", "Mobi -> R$23.990,00", "Cronos -> R$57.990,00\n" };
	public static final String[] MODELOS_FORD = { "Mustang -> R$320.990,00", "Ecosport -> R$49.990,00", "Ka -> R$35.990,00", "Edge -> R$259.990,00\n" };
	public static final int[] RODAS = { 4, 6, 8, 10 };
	public static final int[] PORTAS = { 2, 4};
	public static final int[] CALOTAS = { 8, 12, 16, 20 };	
	
	
	/*
	 * Preço
	 */
	
	public static final double[] PRECO_MARCAS = { 40000, 90000, 15000, 25000 };
	public static final double[] PRECO_COR = { 520, 520, 620, 620 };
	public static final double[] PRECO_MODELOS_BMW = { 462000, 309950, 279563, 419020 };
	public static final double[] PRECO_MODELOS_FERRARI = { 2352000, 924430, 976860, 3252000 };
	public static final double[] PRECO_MODELOS_FIAT = { 67990, 39990, 23990, 57990 };
	public static final double[] PRECO_MODELOS_FORD = { 320990, 49990, 35990, 259990 };
	public static final double PRECO_RODAS = 499.99;
	public static final double PRECO_PORTAS = 620.00;
	public static final double PRECO_CALOTAS = 849.99;
	
	/*
	 * Vendedores
	 */
	public static final String[] VENDEDORES = { "Gabriel", "Guilherme", "Hugo\n" };
}
