package Carros;

/*
 * @author Felype 10/09/2020
 */
public class Carro {
	
	private String marcas;
	private String modelos;
	private int rodas;
	private int portas;
	private int calotas;
	private String cor;
	
	public Carro(String marcas, String modelos, int rodas, int portas, int calotas, String cor) {
		super();
		this.setMarcas(marcas);
		this.setModelos(modelos);
		this.setRodas(rodas);
		this.setPortas(portas);
		this.setCalotas(calotas);
		this.setCor(cor);
	}
	
	public String getMarcas() {
		return marcas;
	}
	
	public void setMarcas(String marcas) {
		this.marcas = marcas;
	}
	
	public String getModelos() {
		return modelos;
	}
	
	public void setModelos(String modelos) {
		this.modelos = modelos;
	}
	
	public int getRodas() {
		return rodas;
	}
	
	public void setRodas(int rodas) {
		this.rodas = rodas;
	}
	
	public int getPortas() {
		return portas;
	}
	
	public void setPortas(int portas) {
		this.portas = portas;
	}
	
	public int getCalotas() {
		return calotas;
	}
	
	public void setCalotas(int calotas) {
		this.calotas = rodas * 2;
	}
	
	public String getCor() {
		return cor;
	}
	
	public void setCor(String cor) {
		this.cor = cor;
	}
	
}
