package Carros;

import java.util.Scanner;
import Config.Settings;

/*
 * @author Felype 10/13/2020
 */

public class Orcamento {
	
	private double carteira = 0;
	
	private boolean bmw = false;
	private boolean ford = false;
	private boolean ferrari = false;
	private boolean fiat = false;

	private boolean modeloSetted = false;
	private boolean rodasSetted = false;
	private boolean portasSetted = false;
	private boolean orcamentoCompleto = false;
	
	private int valorRodas = 0;
	private int valorPortas = 0;
	
	private String valorMenuOrcamento;
	
	Carro caror = new Carro(null,null,0,0,0,null);
	
	public void menuOrcamento() {
		System.out.println("******************************\n");
		System.out.println("1) Iniciar orçamento.");
		System.out.println("2) Ver peças selecionadas.");
		System.out.println("3) Sair.");
		System.out.println("\n******************************");
	}
	
	public String valorMenu() {
		Scanner menuOrcamento = new Scanner(System.in);
		this.valorMenuOrcamento = menuOrcamento.next();
		
		return valorMenuOrcamento;
	}
	
	public void orcamento2() {
		switch(valorMenuOrcamento) {
			case "1":{
				// MARCAS
				System.out.println("\n* Digite o nome da Marca que você deseja:" + "  ***R$"+getCarteira()+"***\n");
				
				for(String marcas : Settings.MARCAS) {
					System.out.println("- "+ marcas);
				}
				
				Scanner pneus = new Scanner(System.in);
				String valorPneus = pneus.next();
				
				if(valorPneus.equals("BMW") || valorPneus.equals("bmw")) {
					caror.setMarcas("BMW");
					bmw = true;
					setCarteira(Settings.PRECO_MARCAS[0]);
					System.out.println("A marca escolhida foi: " +caror.getMarcas());
					
				} else if(valorPneus.equals("FERRARI") || valorPneus.equals("ferrari")) {
					caror.setMarcas("FERRARI");
					ferrari = true;
					setCarteira(Settings.PRECO_MARCAS[1]);
					System.out.println("A marca escolhida foi: " +caror.getMarcas());
					
				} else if(valorPneus.equals("FIAT") || valorPneus.equals("fiat")) {
					caror.setMarcas("FIAT");
					fiat = true;
					setCarteira(Settings.PRECO_MARCAS[2]);
					System.out.println("A marca escolhida foi: " +caror.getMarcas());
					
				} else if(valorPneus.equals("FORD") || valorPneus.equals("ford")) {
					caror.setMarcas("FORD");
					ford = true;
					setCarteira(Settings.PRECO_MARCAS[3]);
					System.out.println("A marca escolhida foi: " +caror.getMarcas());
					
				} else {
					System.err.println("Digite uma marca válida.");
				}
				
				// MODELOS
				System.out.println("\n* Digite o nome do Modelo que você deseja:" + "  ***R$"+getCarteira()+"***\n");
				
				if (bmw == true) {
					for(String modelos : Settings.MODELOS_BMW) {
						System.out.println("- "+ modelos);
					}
						Scanner modelosBmw = new Scanner(System.in);
						String valorModelosBmw = modelosBmw.next();
						
						if(valorModelosBmw.equals("X5") || valorModelosBmw.equals("x5")) {
							caror.setModelos("X5");
							modeloSetted = true;
							setCarteira(getCarteira() + Settings.PRECO_MODELOS_BMW[0]);
							System.out.println("O modelo escolhido foi: " +caror.getModelos());
							
						} else if(valorModelosBmw.equals("Z4") || valorModelosBmw.equals("z4")) {
							caror.setModelos("Z4");
							modeloSetted = true;
							setCarteira(getCarteira() + Settings.PRECO_MODELOS_BMW[1]);
							System.out.println("O modelo escolhido foi: " +caror.getModelos());
							
						} else if(valorModelosBmw.equals("I3") || valorModelosBmw.equals("i3")) {
							caror.setModelos("I3");
							modeloSetted = true;
							setCarteira(getCarteira() + Settings.PRECO_MODELOS_BMW[2]);
							System.out.println("O modelo escolhido foi: " +caror.getModelos());
							
						} else if(valorModelosBmw.equals("X4") || valorModelosBmw.equals("x4")) {
							caror.setModelos("X4");
							modeloSetted = true;
							setCarteira(getCarteira() + Settings.PRECO_MODELOS_BMW[3]);
							System.out.println("O modelo escolhido foi: " +caror.getModelos());
						} else {
							System.err.println("Você não selocionou um modelo.");
						}
						
				} else if (ferrari == true) {
					for(String modelos2 : Settings.MODELOS_FERRARI) {
						System.out.println("- "+ modelos2);
					}
					
						Scanner modelosFerrari = new Scanner(System.in);
						String valorModelosFerrari = modelosFerrari.next();
						
						if(valorModelosFerrari.equals("488")) {
							caror.setModelos("488");
							modeloSetted = true;
							setCarteira(getCarteira() + Settings.PRECO_MODELOS_FERRARI[0]);
							System.out.println("O modelo escolhido foi: " +caror.getModelos());
							
						} else if(valorModelosFerrari.equals("Portofino") || valorModelosFerrari.equals("portofino")) {
							caror.setModelos("Portofino");
							modeloSetted = true;
							setCarteira(getCarteira() + Settings.PRECO_MODELOS_FERRARI[1]);
							System.out.println("O modelo escolhido foi: " +caror.getModelos());
							
						} else if(valorModelosFerrari.equals("F8") || valorModelosFerrari.equals("f8")) {
							caror.setModelos("F8");
							modeloSetted = true;
							setCarteira(getCarteira() + Settings.PRECO_MODELOS_FERRARI[2]);
							System.out.println("O modelo escolhido foi: " +caror.getModelos());
							
						} else if(valorModelosFerrari.equals("GTC4") || valorModelosFerrari.equals("gtc4")) {
							caror.setModelos("GTC4");
							modeloSetted = true;
							setCarteira(getCarteira() + Settings.PRECO_MODELOS_FERRARI[3]);
							System.out.println("O modelo escolhido foi: " +caror.getModelos());
							
						} else {
							System.err.println("Você não selocionou uma marca.");
						}
					
				} else if (fiat == true) {
					for(String modelos3 : Settings.MODELOS_FIAT) {
						System.out.println("- "+ modelos3);
					}
					
						Scanner modelosFiat = new Scanner(System.in);
						String valorModelosFiat = modelosFiat.next();
						
						if(valorModelosFiat.equals("DOBLÓ") || valorModelosFiat.equals("Dobló") || 
								valorModelosFiat.equals("dobló") || valorModelosFiat.equals("doblo")) {
							caror.setModelos("Dobló");
							modeloSetted = true;
							setCarteira(getCarteira() + Settings.PRECO_MODELOS_FIAT[0]);
							System.out.println("O modelo escolhido foi: " +caror.getModelos());
							
						} else if(valorModelosFiat.equals("Argo") || valorModelosFiat.equals("argo")) {
							caror.setModelos("Argo");
							modeloSetted = true;
							setCarteira(getCarteira() + Settings.PRECO_MODELOS_FIAT[1]);
							System.out.println("O modelo escolhido foi: " +caror.getModelos());
							
						} else if(valorModelosFiat.equals("Mobi") || valorModelosFiat.equals("mobi")) {
							caror.setModelos("Mobi");
							modeloSetted = true;
							setCarteira(getCarteira() + Settings.PRECO_MODELOS_FIAT[2]);
							System.out.println("O modelo escolhido foi: " +caror.getModelos());
							
						} else if(valorModelosFiat.equals("Cronos") || valorModelosFiat.equals("cronos")) {
							caror.setModelos("Cronos");
							modeloSetted = true;
							setCarteira(getCarteira() + Settings.PRECO_MODELOS_FIAT[3]);
							System.out.println("O modelo escolhido foi: " +caror.getModelos());
			
						} else {
							System.err.println("Você não selocionou uma marca.");
						}
					
				} else if (ford == true) {
					for(String modelos4 : Settings.MODELOS_FORD) {
						System.out.println("- "+ modelos4);
					}
					
					Scanner modelosFord = new Scanner(System.in);
					String valorModelosFord = modelosFord.next();
					
					if(valorModelosFord.equals("Mustang") || valorModelosFord.equals("mustang")) {
						caror.setModelos("Mustang");
						modeloSetted = true;
						setCarteira(getCarteira() + Settings.PRECO_MODELOS_FORD[0]);
						System.out.println("O modelo escolhido foi: " +caror.getModelos());
						
					} else if(valorModelosFord.equals("Ecosport") || valorModelosFord.equals("ecosport")) {
						caror.setModelos("Ecosport");
						modeloSetted = true;
						setCarteira(getCarteira() + Settings.PRECO_MODELOS_FIAT[1]);
						System.out.println("O modelo escolhido foi: " +caror.getModelos());
						
					} else if(valorModelosFord.equals("Ka") || valorModelosFord.equals("ka")) {
						caror.setModelos("Ka");
						modeloSetted = true;
						setCarteira(getCarteira() + Settings.PRECO_MODELOS_FIAT[2]);
						System.out.println("O modelo escolhido foi: " +caror.getModelos());
						
					} else if(valorModelosFord.equals("Edge") || valorModelosFord.equals("edge")) {
						caror.setModelos("Edge");
						modeloSetted = true;
						setCarteira(getCarteira() + Settings.PRECO_MODELOS_FIAT[3]);
						System.out.println("O modelo escolhido foi: " +caror.getModelos());
						
					}  else {
						System.err.println("Você não selocionou uma marca.");
					}
				}
				
				// Rodas
				
				if (modeloSetted == true) {
					System.out.println("\n* Digite a quantidade de rodas que você deseja: ( 4 à 8 -> R$420,00 por roda )" + 
																						  "  ***R$"+getCarteira()+"***\n");
					
					Scanner rodas = new Scanner(System.in);
					int valorRodas = rodas.nextInt();
					
					if (valorRodas >= 4 && valorRodas <= 8 ) {
						caror.setRodas(valorRodas);
						rodasSetted = true;
						System.out.println("Foram escolhidas " +caror.getRodas()+ " rodas.");
						
						switch(valorRodas) {
						 	case 4:{
						 		setCarteira(getCarteira() + Settings.PRECO_RODAS * 4);
						 		break;
						 	}
						 	
						 	case 5:{
						 		setCarteira(getCarteira() + Settings.PRECO_RODAS * 5);
						 		break;
						 	}
						 	
						 	case 6:{
						 		setCarteira(getCarteira() + Settings.PRECO_RODAS * 6);
						 		break;
						 	}
						 	
						 	case 7:{
						 		setCarteira(getCarteira() + Settings.PRECO_RODAS * 7);
						 		break;
						 	}
						 	
						 	case 8:{
						 		setCarteira(getCarteira() + Settings.PRECO_RODAS * 8);
						 		break;
						 	}
						}
					} else {
						System.err.println("Escolha um número válido de rodas.");
					}
				}
				
				// Calotas
				
				if (rodasSetted == true) {
					caror.setCalotas(caror.getRodas());
						switch(valorRodas) {
					 	case 4:{
					 		setCarteira(getCarteira() + Settings.PRECO_CALOTAS * 8);
					 		break;
					 	}
					 	
					 	case 5:{
					 		setCarteira(getCarteira() + Settings.PRECO_CALOTAS * 10);
					 		break;
					 	}
					 	
					 	case 6:{
					 		setCarteira(getCarteira() + Settings.PRECO_CALOTAS * 12);
					 		break;
					 	}
					 	
					 	case 7:{
					 		setCarteira(getCarteira() + Settings.PRECO_CALOTAS * 14);
					 		break;
					 	}
					 	
					 	case 8:{
					 		setCarteira(getCarteira() + Settings.PRECO_CALOTAS * 16);
					 		break;
					 	}
					  }
				} else {
					System.err.println("error");
				}
				
				// Portas
				
				if (rodasSetted == true) {
					System.out.println("\n* Digite a quantidade de portas que você deseja: ( 2 à 4 R$620,00 por porta)." + 
																						 "  ***R$"+getCarteira()+"***\n");
					
					Scanner portas = new Scanner(System.in);
					int valorPortas = portas.nextInt();
					
					if (valorPortas >= 2 && valorPortas <= 4 ) {
						caror.setPortas(valorPortas);
						portasSetted = true;
							switch(valorPortas) {
						 	case 2:{
						 		setCarteira(getCarteira() + Settings.PRECO_PORTAS * 2);
						 		break;
						 	}
						 	
						 	case 3:{
						 		setCarteira(getCarteira() + Settings.PRECO_PORTAS * 3);
						 		break;
						 	}
						 	
						 	case 4:{
						 		setCarteira(getCarteira() + Settings.PRECO_PORTAS * 4);
						 		break;
						 	}
						  }
						System.out.println("Foram escolhidas " +caror.getPortas()+ " portas.");
					} else {
						System.err.println("Escolha um número válido de portas.");
					}
				}
				
				// Cor
				
				if (portasSetted == true) {
					System.out.println("\n* Digite a cor que você deseja para seu automóvel:" + "  ***R$"+getCarteira()+"***\n");
					orcamentoCompleto = true;
					for(String cores : Settings.COR) {
						System.out.println(cores);
					}
					Scanner cor = new Scanner(System.in);
					String valorCor = cor.next();
					
					if(valorCor.equals("branco") || valorCor.equals("BRANCO")) {
						caror.setCor("BRANCO");
						setCarteira(getCarteira() + Settings.PRECO_COR[0]);
						System.out.println("A cor escolhida foi: " +caror.getCor());
					} 
					
					else if(valorCor.equals("preto") || valorCor.equals("PRETO")) {
						caror.setCor("PRETO");
						setCarteira(getCarteira() + Settings.PRECO_COR[1]);
						System.out.println("A cor escolhida foi: " +caror.getCor());
					}
					
					else if(valorCor.equals("vermelho") || valorCor.equals("VERMELHO")) {
						caror.setCor("VERMELHO");
						setCarteira(getCarteira() + Settings.PRECO_COR[2]);
						System.out.println("A cor escolhida foi: " +caror.getCor());
					}
					
					else if(valorCor.equals("azul") || valorCor.equals("AZUL")) {
						caror.setCor("AZUL");
						setCarteira(getCarteira() + Settings.PRECO_COR[3]);
						System.out.println("A cor escolhida foi: " +caror.getCor());
					}
				} else {
					System.err.println("error");
				}
				
				break;
			}
			case "2":{
				if (orcamentoCompleto == true) {
					System.out.println("Aqui estão as peças selecionadas: ***"+getCarteira()+"***\n");
					System.out.println("- Marca: " +caror.getMarcas());
					System.out.println("- Modelo: " +caror.getModelos());
					System.out.println("- Quantidade de Rodas: " +caror.getRodas());
					System.out.println("- Quantidade de Calotas: " +caror.getCalotas());
					System.out.println("- Quantidade de Portas: " +caror.getPortas());
					System.out.println("- Cor: " +caror.getCor());
				} else {
					System.err.println("Você precisa fazer um orçamento primeiro.");
				}

				break;
			}
			case "3":{
				System.exit(0);
				break;
			}
			
			default:{
				System.err.println("\nEscolha um opção válida");
				break;
			}
		}
	}
	
	public double getCarteira() {
		return carteira;
	}
	
	public void setCarteira(double carteira) {
		this.carteira = carteira;
	}
}
