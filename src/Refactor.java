import java.util.Random;
import java.util.Scanner;

import Carros.Orcamento;
import Config.Settings;
import Vendedores.Gabriel;
import Vendedores.Guilherme;
import Vendedores.Hugo;

/*
 * @author Felype 10/10/2020
 */
public class Refactor {

	public Refactor() {
		if(Launcher.vendedorSetted == false) {
			execucaoMenu();
		} else {
			execucaoMenu2();
		}
	}

	Random random = new Random();
	Guilherme gui = new Guilherme();
	Gabriel gab = new Gabriel();
	Hugo hug = new Hugo();
	
	/*
	 * Menu para quem não tem vendedor
	 */

	public void execucaoMenu() {
		switch(Launcher.valorDecisaoMenu) {
		case "1":{
			System.out.println("\nFique a vontade para escolher um de nossos vendedores.\n");
			for (String vendedores : Settings.VENDEDORES) {
				System.out.println("-Vendedor "+vendedores);
			}

			Scanner escolherVendedor = new Scanner (System.in);
			String valorEscolherVendedor = escolherVendedor.nextLine();

			if (valorEscolherVendedor.equals("Gabriel") || valorEscolherVendedor.equals("gabriel")) {
				Launcher.vendedorEscolhido = Settings.VENDEDORES[0];
				Launcher.vendedorSetted = true;
				gab.starterGabriel();
			} else if (valorEscolherVendedor.equals("Guilherme") || valorEscolherVendedor.equals("guilherme")) {
				Launcher.vendedorEscolhido = Settings.VENDEDORES[1];
				Launcher.vendedorSetted = true;
				gui.starterGuilherme();
			} else if (valorEscolherVendedor.equals("Hugo") || valorEscolherVendedor.equals("hugo")) {
				Launcher.vendedorEscolhido = Settings.VENDEDORES[2];
				Launcher.vendedorSetted = true;
				hug.starterHugo();
			} else {
				System.err.println("Este vendendor não está disponvel no momento.");
			}

			break;
		}
		case "2":{
			if(Launcher.vendedorSetted == false) {
				System.err.println("Para perguntar as marcas disponíveis você precisa escolher um vendedor primeiro.\n");
			} else {
				for (String marcas : Settings.MARCAS) {
					System.out.println("- " +marcas);
				}
			}
			break;
		}
		case "3":{
			if(Launcher.vendedorSetted == false) {
				System.err.println("Para fazer um orçamento você precisa escolher um vendedor primeiro.\n");
			} else {
				boolean loopingTeste = true;
				Orcamento orc = new Orcamento();
				while(loopingTeste) {
					orc.menuOrcamento();
					orc.valorMenu();
					orc.orcamento2();
				}
			}
			break;
		}
		case "4":{
			if(Launcher.vendedorSetted == false) {
				System.err.println("Para pedir os meios de contato você precisa escolher um vendedor primeiro.\n");
			} else {
				System.out.println("Aqui está " +Launcher.nomeDoCliente+ ".\n");
				for (String contatos : Settings.CONTATO) {
					System.out.println("- "+contatos);
				}
			}
			break;
		}
		case "5":{
			System.out.println("Um vendedor lhe aborda..");
			System.out.println("Posso te ajudar de alguma maneira?\n");
			System.out.println("1) Não não.. já estou de saída.");
			System.out.println("2) Estou apenas dando uma olhadinha");

			Scanner finalizando = new Scanner(System.in);
			String valorFinalizando = finalizando.next();

			switch(valorFinalizando) {
			case "1":{
				int numeroRandom = random.nextInt(Settings.PENSAMENTOS.length);
				System.out.print(Settings.PENSAMENTOS[numeroRandom]); 
				System.out.print(" Melhor eu ir andando.");;
				System.exit(0);
				break;
			}

			case "2":{
				System.out.println("\nTudo bem, sem problema, antes de ir, o senhor gostaria de um meio de contato?");
				System.out.println("1) Sim.. por favor.");
				System.out.println("2) Não não.. agradeço.");

				Scanner finalizando2 = new Scanner(System.in);
				String valorFinalizando2 = finalizando2.next();

				if(valorFinalizando2.equals("1")) {
					System.out.println("Aqui está " +Launcher.nomeDoCliente+ ".\n");
					for (String contatos : Settings.CONTATO) {
						System.out.println("- "+contatos);
					}
					System.exit(0);
					
				} else {
					System.out.print("Melhor eu ir andando.");;
					System.exit(0);
				}
				break;
			}
			}
			break;
		}
		default:{
			System.err.println("\nEscreva uma opção válida");
			}
		}
	}
	
	public void execucaoMenu2() {
			switch(Launcher.valorDecisaoMenu2) {
			case "1":{
				for (String marcas : Settings.MARCAS) {
					System.out.println("- " +marcas);
				}
				break;
			}
			case "2":{
				System.out.println("Olá!");
				break;
			}
			case "3":{
				System.out.println("Aqui está " +Launcher.nomeDoCliente+ ".\n");
				for (String contatos : Settings.CONTATO) {
					System.out.println("- "+contatos);
				}
				break;
			}
			case "4":{
				int numeroRandom = random.nextInt(Settings.PENSAMENTOS.length);
				System.out.print(Settings.PENSAMENTOS[numeroRandom]); 
				System.out.print(" Melhor eu ir andando.");;
				System.exit(0);
				break;
			}
			default:{
				System.err.println("\nEscreva uma opção válida");
				}
		}
	}
}
